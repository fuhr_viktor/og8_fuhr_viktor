public class Stoppuhr {
	private static long startzeit;
	private static long endzeit;

	public static void start() {
		startzeit = System.currentTimeMillis();
	}

	public static void stopp() {
		endzeit = System.currentTimeMillis();
	}

	public static void reset() {
		startzeit = 0;
		endzeit = 0;
	}

	public static long getDauerInMs() {
		return endzeit - startzeit;
	}
}
