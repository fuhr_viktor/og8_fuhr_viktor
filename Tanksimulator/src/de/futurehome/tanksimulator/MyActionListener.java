package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();
			if (f.myTank.getFuellstand() < 100) {
			fuellstand = fuellstand + 5;
			f.myTank.setFuellstand(fuellstand);
			f.pbFuellstand.setValue((int) fuellstand);
			}
			f.lblFuellstand.setText("" + fuellstand + "%" + " | " + fuellstand + "L");
			System.out.println("Der Füllstand ist um 5.0L gestiegen | " + LocalTime.now());
		}

		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			if (f.myTank.getFuellstand() >= f.sldVerbrauch.getValue()) {
				fuellstand = fuellstand - f.sldVerbrauch.getValue();
				f.myTank.setFuellstand(fuellstand);
				f.pbFuellstand.setValue((int) fuellstand);
				System.out.println("Der Füllstand ist um " +  f.sldVerbrauch.getValue() +  ".0L gesunken | " + LocalTime.now());
			}
			f.lblFuellstand.setText("" + fuellstand + "%" + " | " + fuellstand + "L");
			
		}

		if (obj == f.btnZurueksetzen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);
			f.pbFuellstand.setValue((int) fuellstand);
			f.lblFuellstand.setText("" + fuellstand + "%" + " | " + fuellstand + "L");
		}
		
	}
}