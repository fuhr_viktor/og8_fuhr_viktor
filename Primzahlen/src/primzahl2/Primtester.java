package primzahl2;

public class Primtester {
	public boolean isPrim(long zahl) {
		if (zahl < 2)
			return false;
		for (long i = 2; i < zahl; i++) {
			if (zahl % i == 0) {
				return false;
			}
		}
		return true;
	}

}
