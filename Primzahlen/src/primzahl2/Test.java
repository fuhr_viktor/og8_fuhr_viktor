package primzahl2;

public class Test {

	public static void main(String[] args) {
		Primtester p01 = new Primtester();

		Stoppuhr s01 = new Stoppuhr();

		for (long i = 2_000_000_000l; i < 20_000_000_000l; i += 2_000_000_000l) {
			s01.start();
			System.out.println(p01.isPrim(i));
			s01.stopp();

			System.out.println(s01.getDauerInMs());
		}
System.out.println("ende");
	}

}
