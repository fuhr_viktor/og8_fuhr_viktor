package primzahlen;

public class Rechner {
	private long restZaehler = 0;
	private long rest;
	private long i = 2;

	/*
	 * public void reset() {
	 * restZaehler = 0; rest = 0; i = 2; 
	 * }
	 */
	public boolean ueberpruefeAufPrim(long zahl) {
		restZaehler = 0;
		rest = 0;
		i = 2;
		// reset();
		if (zahl == 2 || zahl == 3) {
			// reset();
			return true;
		}
		do {
			rest = zahl % i;
			if (rest >= 1) {
				restZaehler++;
			}
			i++;
			if (rest == 0) {
				// reset();
				return false;
			}
		} while (i < zahl - 1);
		// reset();
		return (zahl != (restZaehler + 2));
	}

}
