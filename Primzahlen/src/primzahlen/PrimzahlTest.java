package primzahlen;

public class PrimzahlTest {

	public static void main(String[] args) {

		Rechner r01 = new Rechner();

		System.out.println(r01.ueberpruefeAufPrim(989127));
		System.out.println(r01.ueberpruefeAufPrim(989123));
		System.out.println(r01.ueberpruefeAufPrim(9));
		System.out.println(r01.ueberpruefeAufPrim(6));
		System.out.println(r01.ueberpruefeAufPrim(1));
		System.out.println(r01.ueberpruefeAufPrim(9999999929L));
	}

}
