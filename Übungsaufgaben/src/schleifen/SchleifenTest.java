package schleifen;

//import eines Scanners 
import java.util.Scanner;

public class SchleifenTest {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		int anzahl;
		System.out.println("Bitte geben sie eine Zahl ein: ");
		anzahl = scan.nextInt();

		//Plus mit for-Schleife
		for (int i = 0; i < anzahl; i++) {
			System.out.print("+");
		}
		
		System.out.println();
		
		//Minus mit kopfgesteuerter Schleife
		int i = 0;
		while (i < anzahl) {
			System.out.print("-");
			i++;
		}
		
		System.out.println();
		//Sternchen mit Fu�gesteuerter Schleife
		i = 0;
		do {
			System.out.print("*");
			i++;
		} while (i < anzahl);
	}
}
