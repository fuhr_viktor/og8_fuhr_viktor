package taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		@SuppressWarnings("resource")
		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int eingabe1;
		int eingabe2;
		int swValue;

		// Display menu graphics
		
		System.out.print("Zahl 1: ");
		eingabe1 = myScanner.nextInt();
		System.out.print("Zahl 2: ");
		eingabe2 = myScanner.nextInt();
		
		
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Multiplizieren |");
		System.out.println("|        4. Dividieren     |");
		System.out.println("|        5. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = myScanner.next().charAt(0);

		// Switch construct
		switch (swValue) {
		case '1':
			System.out.println(eingabe1 + " + " + eingabe2 + " = " + ts.add(eingabe1, eingabe2));
			break;
		case '2':
			System.out.println(eingabe1 + " - " + eingabe2 + " = " + ts.sub(eingabe1, eingabe2));
			break;	
		case '3':
			System.out.println(eingabe1 + " * " + eingabe2 + " = " + ts.mul(eingabe1, eingabe2));
			break;	
		case '4':
			if(eingabe2 == 0) {
				System.out.println("Fehler: Division durch 0 nicht m�glich!");
			}else{
				System.out.println(eingabe1 + " / " + eingabe2 + " = " + ts.div(eingabe1, eingabe2));
			}
			break;
		case '5':
			System.exit(0);
			break;
		 
		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}

	}

}
