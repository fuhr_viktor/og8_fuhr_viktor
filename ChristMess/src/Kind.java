import java.util.*;

public class Kind {
	String nachname;
	String vorname;
	String name;
	String geburtstag;
	int bravheitsgrad;
	String wohnohrt;

	public Kind(String name, String geburtstag, int bravheitsgrad, String wohnohrt) {
		String[] split = name.split(" " );
		this.vorname = split[0];
		this.nachname = split[1];
		this.geburtstag = geburtstag;
		this.bravheitsgrad = bravheitsgrad;
		this.wohnohrt = wohnohrt;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getGeburtstag() {
		return geburtstag;
	}

	public void setGeburtstag(String geburtstag) {
		this.geburtstag = geburtstag;
	}

	public String getWohnohrt() {
		return wohnohrt;
	}

	public void setWohnohrt(String wohnohrt) {
		this.wohnohrt = wohnohrt;
	}

	public int getBravheitsgrad() {
		return bravheitsgrad;
	}

	public void setBravheitsgrad(int bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}

	@Override
	public String toString() {
		return "Kind [vorname=" + vorname + ", nachname=" + nachname + ", geburtstag=" + geburtstag + ", bravheitsgrad=" + bravheitsgrad + ", wohnohrt="
				+ wohnohrt + "]";
	}

	// Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und
	// Bravheitsgrad
	// Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine
	// toString-Methode

}
