import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class KindGenerator {
	
	ArrayList<Kind> list = new ArrayList<Kind>();
	
	private File file;

	KindGenerator(File file) {
		this.file = file;
	}

	public ArrayList<Kind> generiere() {
		try {
			FileReader fileReader = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fileReader);
			String line;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split(", ");
				list.add(new Kind(parts[0], parts[1], Integer.parseInt(parts[2]), parts[3]));
			}

		} catch (Exception e) {
			System.out.println("keine Datei gefunden");
			e.printStackTrace();
		}
		return list;
	}

}