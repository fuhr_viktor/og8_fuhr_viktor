import java.util.Comparator;

public class SortBravheit implements Comparator<Kind> {
	@Override
	public int compare(Kind k1, Kind k2) {
		return k1.getBravheitsgrad() - k2.getBravheitsgrad();
	}
}