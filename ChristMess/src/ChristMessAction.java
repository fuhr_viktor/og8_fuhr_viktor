import java.io.File;
import java.util.*;

public class ChristMessAction {

	public static void main(String[] args) {

		File file = new File("kinddaten.txt");
		KindGenerator kg = new KindGenerator(file);
		// ArrayList<Kind> kinder = kg.generiere();
		List<Kind> kinder = new ArrayList<Kind>();
		kinder = kg.generiere();

		Collections.sort(kinder, new SortNachname());
		kinder = Sorter.sortier(kinder);
		
		System.out.println("Sortierung nach Nachname:");
		for (Kind i : kinder) {
			System.out.println(i);
		}

	}

}
