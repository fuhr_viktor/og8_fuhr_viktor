import java.util.List;

public class Sorter {

	public static List<Kind> sortier(List<Kind> kinder) {
		int niedrigsterBGIndex = 0;
		int letzterWert = kinder.size();
		while (letzterWert > 0) {
			for (int i = 0; i < letzterWert; i++) {
				if (kinder.get(niedrigsterBGIndex).getBravheitsgrad() < kinder.get(i).getBravheitsgrad())
					niedrigsterBGIndex = i;
			}
			Kind temp = kinder.get(niedrigsterBGIndex);
			kinder.remove(niedrigsterBGIndex);
			kinder.add(temp);
			letzterWert--;
		}
		return kinder;
	}

}