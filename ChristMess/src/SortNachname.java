import java.util.Comparator;

public class SortNachname implements Comparator<Kind> {
	@Override
	public int compare(Kind k1, Kind k2) {
		return k1.getNachname().compareTo(k2.getNachname());
	}
}