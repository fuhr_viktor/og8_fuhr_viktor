package zooTycoon;

public class Addon {

	private int idNummer;
	private double kosten;
	private String name;
	private int bestand;
	private int maxBestand;


	public Addon() {
		idNummer = 0;
		kosten = 0;
		name = "";
		bestand = 0;
		maxBestand = 0;
	}

	public Addon(int idNummer, double kosten, String name, int bestand, int maxBestand) {
		this.idNummer = idNummer;
		this.kosten = kosten;
		this.name = name;
		this.bestand = bestand;
		this.maxBestand = maxBestand;
	}
	
	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummerNeu) {
		idNummer = idNummerNeu;
	}

	public double getKosten() {
		return kosten;
	}

	public void setKosten(double kostenNeu) {
		kosten = kostenNeu;
	}

	public String getName() {
		return name;
	}

	public void setName(String nameNeu) {
		name = nameNeu;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestandNeu) {
		bestand = bestandNeu;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestandNeu) {
		maxBestand = maxBestandNeu;
	}

	public void kaufen() {
		// Quelltext
	}

	public void verbrauchen() {
		// Quelltext
	}

	public void aendereBestand(int anzahl) {
		if (anzahl <= maxBestand) {
			bestand = anzahl;
		}
	}

	public double berechneGesamtwert() {
		return bestand * kosten;
	}
}