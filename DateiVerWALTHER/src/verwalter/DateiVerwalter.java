package verwalter;

import java.io.*;

public class DateiVerwalter {

	private File file;

	DateiVerwalter(File file) {
		this.file = file;
	}

	public void lesen() {
		try {
			FileReader fillReader = new FileReader(this.file);
			BufferedReader bufFREDReader = new BufferedReader(fillReader);
			String s;
			while ((s = bufFREDReader.readLine()) != null) {
				System.out.println(s);
			}
			bufFREDReader.close();
		} catch (Exception e) {
			System.out.println("Fred hat keine Datei gefunden");
			e.printStackTrace();
		}

	}

	public void schreibe(String s) {
		try {
			FileWriter fillWriter = new FileWriter(this.file, true);
			BufferedWriter buFREDWriter = new BufferedWriter(fillWriter);
			buFREDWriter.write(s);
			buFREDWriter.flush();
			buFREDWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
	}

	public void schreibeln(String s) {
		try {
			FileWriter fillWriter = new FileWriter(this.file, true);
			BufferedWriter buFREDWriter = new BufferedWriter(fillWriter);
			buFREDWriter.write(s);
			buFREDWriter.newLine();
			buFREDWriter.flush();
			buFREDWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}

	}

	public void leere() {
		try {
			FileWriter fillWriter = new FileWriter(this.file);
			BufferedWriter buFREDWriter = new BufferedWriter(fillWriter);
			buFREDWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		File file = new File("Test.txt");
		DateiVerwalter dv = new DateiVerwalter(file);
		dv.leere();
	}

}