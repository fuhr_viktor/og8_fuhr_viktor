package view;

import java.awt.Choice;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import musikvoting.LoginSystem;
import musikvoting.Playlist;

public class VotingFenster extends JFrame {

	AddFenster af = new AddFenster(this);
	PlaylistFenster pf = new PlaylistFenster(this);
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VotingFenster frame = new VotingFenster();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VotingFenster() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 380, 300);
		contentPane = new JPanel() {
			public void paintComponent(Graphics g) {
				Image img = Toolkit.getDefaultToolkit()
						.getImage(LoginFenster.class.getResource("/Bilder/bg-Voting.jpg"));
				g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
			}
		};
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 150, 208, 0 };
		gbl_contentPane.rowHeights = new int[] { 40, 40, 40, 122, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		Choice choice = new Choice();
		for (int i = 0; i < Playlist.getLaenge(); i++) {
			choice.add(Playlist.getSong(i).getTitel());
		}
		GridBagConstraints gbc_choice = new GridBagConstraints();
		gbc_choice.fill = GridBagConstraints.HORIZONTAL;
		gbc_choice.insets = new Insets(0, 0, 5, 0);
		gbc_choice.gridx = 1;
		gbc_choice.gridy = 0;
		contentPane.add(choice, gbc_choice);

		JButton btnVote = new JButton("Voten");
		btnVote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Playlist.getSong(choice.getSelectedItem()).voten();
					JOptionPane.showMessageDialog(null, "Dein Song wurde gevotet!", "Voted!", JOptionPane.OK_OPTION);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Kein Song vorhanden!", "nicht voted!", JOptionPane.OK_OPTION);
				}
			}
		});
		GridBagConstraints gbc_btnVote = new GridBagConstraints();
		gbc_btnVote.fill = GridBagConstraints.VERTICAL;
		gbc_btnVote.insets = new Insets(0, 0, 5, 5);
		gbc_btnVote.gridx = 0;
		gbc_btnVote.gridy = 0;
		contentPane.add(btnVote, gbc_btnVote);

		JButton btnAdd = new JButton("Song Hinzuf\u00FCgen");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				af.pack();
				af.setVisible(true);
				dispose();
			}
		});

		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.fill = GridBagConstraints.VERTICAL;
		gbc_btnAdd.insets = new Insets(0, 0, 5, 5);
		gbc_btnAdd.gridx = 0;
		gbc_btnAdd.gridy = 1;
		contentPane.add(btnAdd, gbc_btnAdd);

		JButton btnPlaylist = new JButton("Playlist");
		btnPlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (LoginSystem.getEingeloggt().getTyp().equals("dj")) {
					pf.pack();
					pf.setVisible(true);
					dispose();
				} else {
					JOptionPane.showMessageDialog(null, "Nur Dj's k�nnen auf die Playlist zugreifen!", "Fehler",
							JOptionPane.OK_OPTION);
				}
			}
		});
		GridBagConstraints gbc_btnPlaylist = new GridBagConstraints();
		gbc_btnPlaylist.fill = GridBagConstraints.VERTICAL;
		gbc_btnPlaylist.insets = new Insets(0, 0, 5, 5);
		gbc_btnPlaylist.gridx = 0;
		gbc_btnPlaylist.gridy = 2;
		contentPane.add(btnPlaylist, gbc_btnPlaylist);

		JTextArea txtaVotes = new JTextArea(Playlist.getTop5());
		txtaVotes.setEditable(false);
		GridBagConstraints gbc_txtaVotes = new GridBagConstraints();
		gbc_txtaVotes.anchor = GridBagConstraints.NORTHWEST;
		gbc_txtaVotes.gridwidth = 2;
		gbc_txtaVotes.insets = new Insets(0, 0, 0, 5);
		gbc_txtaVotes.fill = GridBagConstraints.BOTH;
		gbc_txtaVotes.gridx = 0;
		gbc_txtaVotes.gridy = 3;
		contentPane.add(txtaVotes, gbc_txtaVotes);

		JButton btnUpdate = new JButton("Aktualisieren");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Playlist.sortieren();
				txtaVotes.setText(Playlist.getTop5());
				choice.removeAll();
				for (int i = 0; i < Playlist.getLaenge(); i++) {
					choice.add(Playlist.getSong(i).getTitel());
				}
			}
		});

		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.fill = GridBagConstraints.VERTICAL;
		gbc_btnUpdate.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdate.gridx = 1;
		gbc_btnUpdate.gridy = 2;
		contentPane.add(btnUpdate, gbc_btnUpdate);
	}

}
