package view;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import musikvoting.Playlist;

public class PlaylistFenster extends JFrame {

	private JPanel contentPane;
	static JFrame vf = new JFrame();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlaylistFenster frame = new PlaylistFenster(vf);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PlaylistFenster(JFrame j) {
		vf = j;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 440, 363);
		contentPane = new JPanel() {
			public void paintComponent(Graphics g) {
			Image img = Toolkit.getDefaultToolkit().getImage(  
			LoginFenster.class.getResource("/Bilder/bg-Playlist.jpg"));  
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
			}
		};
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JTextArea txtaPlaylist = new JTextArea(Playlist.string());
		txtaPlaylist.setEditable(false);
		GridBagConstraints gbc_txtaPlaylist = new GridBagConstraints();
		gbc_txtaPlaylist.gridwidth = 2;
		gbc_txtaPlaylist.insets = new Insets(0, 0, 5, 0);
		gbc_txtaPlaylist.fill = GridBagConstraints.BOTH;
		gbc_txtaPlaylist.gridx = 0;
		gbc_txtaPlaylist.gridy = 0;
		getContentPane().add(txtaPlaylist, gbc_txtaPlaylist);
		
		
		JButton btnUpdate = new JButton("Aktualisieren");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtaPlaylist.setText(Playlist.string());
				dispose();				
				pack();
				setVisible(true);
			}
		});
		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.gridx = 0;
		gbc_btnUpdate.gridy = 1;	
		getContentPane().add(btnUpdate, gbc_btnUpdate);
		
		JButton btnZurueck = new JButton("Zur\u00FCck");
		btnZurueck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {		
				setVisible(false);
				j.pack();
				j.setVisible(true);
				dispose();
			}
		});
		GridBagConstraints gbc_btnZurueck = new GridBagConstraints();
		gbc_btnZurueck.gridx = 1;
		gbc_btnZurueck.gridy = 1;
		getContentPane().add(btnZurueck, gbc_btnZurueck);
	}
}
