package musikvoting;

public class Account {
	String typ;
	String name;
	String passwort;
	
	public Account(String name, String passwort, String typ) {
		this.name = name;
		this.passwort = passwort;
		this.typ = typ;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}	
	
	@Override
	public String toString() {
		return "Account [typ=" + typ + ", name=" + name + ", passwort=" + passwort + "]";
	}

}
